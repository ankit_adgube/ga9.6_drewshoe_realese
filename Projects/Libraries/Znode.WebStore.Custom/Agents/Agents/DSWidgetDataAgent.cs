﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSWidgetDataAgent : WidgetDataAgent
    {
        private IWebStoreWidgetClient _widgetClient;

        #region Constructor
        public DSWidgetDataAgent(IWebStoreWidgetClient widgetClient, IPublishProductClient productClient, IPublishCategoryClient publishCategoryClient, IBlogNewsClient blogNewsClient, IContentPageClient contentPageClient, ISearchClient searchClient, ICMSPageSearchClient cmsPageSearchClient) :
            base(widgetClient, productClient, publishCategoryClient, blogNewsClient, contentPageClient, searchClient, cmsPageSearchClient)
        {
            _widgetClient = GetClient<IWebStoreWidgetClient>(widgetClient);
        }
        #endregion

        #region public methods

        public override ProductListViewModel GetCategoryProducts(WidgetParameter widgetparameter, int pageNum = 1, int pageSize = 32, int sortValue = 0)
        {

            pageSize = 32;
            string PropertyValue = Convert.ToString(widgetparameter.properties?.Where(x => x.Key.Contains("pageSize")).FirstOrDefault().Value);
            string SearchTermValue = Convert.ToString(widgetparameter.properties?.Where(x => x.Key.Contains("SearchTerm")).FirstOrDefault().Value);

            if (PropertyValue == "0")
            {
                SaveInSession(WebStoreConstants.ViewAllMode, true);
            }
            else
            {
                SaveInSession(WebStoreConstants.ViewAllMode, false);
            }
            //if (SearchTermValue != null || SearchTermValue != "") {
            SaveInSession("SearchTermValue", SearchTermValue);
            // }

            return base.GetCategoryProducts(widgetparameter, pageNum, pageSize, sortValue);
        }

        #endregion

    }
}
