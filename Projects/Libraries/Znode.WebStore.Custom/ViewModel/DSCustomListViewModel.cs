﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DSCustomListViewModel : BaseViewModel
    {
        public List<DSCustomViewModel> InventoryList { get; set; }
    }
}
