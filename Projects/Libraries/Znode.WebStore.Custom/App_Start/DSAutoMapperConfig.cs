﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Sample.Api.Model;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.App_Start
{
    public static class DSAutoMapperConfig
    {

        public static void Execute()
        {

            // Mapper.CreateMap<DSCustomListModel, DSCustomListViewModel>().ReverseMap();

            Mapper.CreateMap<DSCustomModel, DSCustomViewModel>();

            Mapper.CreateMap<DSCustomViewModel, DSCustomModel>();

        }
    }
}
