﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Client.Expands;
using Znode.Api.Custom.Cache.Cache;

namespace Znode.Api.Custom.Controller.Custom
{
    public class DsCustomController : BaseController
    {
        private readonly IDSCustomCache _cache;
        private readonly IDSCustomService _DSCustomService;

        public DsCustomController(IDSCustomService DSCustomService)
        {
            _DSCustomService = DSCustomService;
            _cache = new DSCustomCache(_DSCustomService);
        }

        /// <summary>
        /// Get Portal List.
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetInventoyGrid(int PublishProductId, string Color)
        //public HttpResponseMessage GetInventoyGrid()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetInventoyGrid(PublishProductId, Color, RouteUri, RouteTemplate);
                response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<string>(data);

                //response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<OrderListResponse>(data);
            }
            catch (Exception ex)
            {
                //CustomPortalDetailListResponse data = new CustomPortalDetailListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse("error");
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Portal.ToString());
            }
            return response;
        }



    }
}
