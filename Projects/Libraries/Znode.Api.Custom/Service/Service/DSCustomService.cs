﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;
using Znode.Sample.Api.Model;

namespace Znode.Api.Custom.Service.Service
{
    public class DSCustomService : BaseService, IDSCustomService
    {

        public DSCustomListModel GetInventoyGrid(int PublishProductId, string Color)
        {
            Color = Color.Replace("$$", "/"); /*replaced / in product.ts with $$$ as query string not support /*/

            IZnodeViewRepository<DSCustomModel> objStoredProc = new ZnodeViewRepository<DSCustomModel>();
            objStoredProc.SetParameter("@PublishProductId", PublishProductId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Color", Color, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            List<DSCustomModel> dSCustomModel = objStoredProc.ExecuteStoredProcedureList("Nivi_InventoryGetChart @PublishProductId,@color").ToList();
            DSCustomListModel DSCustomListModel = new DSCustomListModel { InventoryList = dSCustomModel?.ToList() };
            return DSCustomListModel;
        }
    }
}
