﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;

namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IDSCustomClient : IBaseClient
    {
        //string GetInventoyGrid(int PublishProductId, string Color);
        DSCustomListModel GetInventoyGrid(int PublishProductId, string Color);
    }


}
